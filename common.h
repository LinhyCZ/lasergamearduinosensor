#ifndef COMMON_H
#define COMMON_H

#define I2C_BUFFER_SIZE   10      //Size of the I2C buffer
#define BYTE_CHECK_MASK   0x88FF8800 //Only last two byte gets the value.

#endif
