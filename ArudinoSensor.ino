/*      
*  LaserGame - Arduino Sensor
*  @author - Tomas Linhart
*  @version - 1.0.0
*  @version - 14.4.2022
*  
*  Program for receiving shots from other players. Uses IR receiver to detect the hits.
*  Data are sent to A9G main microcontoller using I2C bus.
*/     
#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include <IRremote.h> 
#include "commands.h"
#include "common.h"

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

#define IR_RECEIVE_PIN    6       //Data pin where IR receiver is connected
#define NEOPIXELS_PIN     4       //Data pin where LEDs are connected
#define NUMPIXELS         5       //Count of LED diodes on the module

#define INTERRUPT_PIN     13      //Data pin that is triggered when data wants to be send
#define I2C_ADDRESS       11      //I2C Address of current module. Has to be different for every module connected to A9G. Address has to be in range from 10 to 15.

#define DEBUG             0       //Debug flag.

//Buffer for storing I2C data
byte valueToSend[I2C_BUFFER_SIZE] = "";

//Initialize Neopixels object
Adafruit_NeoPixel pixels(NUMPIXELS, NEOPIXELS_PIN, NEO_RGB + NEO_KHZ800);

//All team colors available to show on LEDs
uint32_t teamColors[9] = {
  pixels.Color(255, 255, 255),  //WHITE
  pixels.Color(255, 0, 0),      //RED
  pixels.Color(0, 255, 0),      //GREEN
  pixels.Color(0, 0, 255),      //BLUE
  pixels.Color(255, 255, 0),    //YELLOW
  pixels.Color(0, 255, 255),    //CYAN
  pixels.Color(255, 0, 255),    //MAGENTA
  pixels.Color(255, 64, 0),     //ORANGE
  pixels.Color(0, 0, 0)         //BLACK
};

//Game information - If vest is activated and should read values from IR sensor and number of team.
bool isActivated = false;
uint8_t team = 8; // Set to off by default

/* ____________________________________________________________________________

    void handleRequest()

    Function for handling request for I2C data. Sends value from valueToSend 
    buffer and sets it to NOINFO action.
   ____________________________________________________________________________
*/
void handleRequest() {
  debug("Got request for data", true);

  //Write all data from valueToSend buffer;
  for (int i = 0; i < I2C_BUFFER_SIZE; i++) {
    Wire.write(valueToSend[i]);
  }

  
  //Empty the buffer and set it to NOINFO command.
  memset(valueToSend, 0, sizeof(valueToSend));
  valueToSend[0] = NOINFO;


  //debug
  debug("Setting command to NOINFO.", true);
  debug("Empty done.", true);
}

/* ____________________________________________________________________________

    void handleReceive(int bytesToRead)

    Function for receiving I2C messages. Reads all data and does an action 
    based on received command in first byte of the message.
   ____________________________________________________________________________
*/
void handleReceive(int bytesToRead) {
  //Read all bytes from I2C
  byte receivedData[I2C_BUFFER_SIZE] = "";
  int readByte = 0;
  
  while (Wire.available()) {
    byte c = Wire.read();

    if (c != 0x00) {
      receivedData[readByte] = c; 
      readByte++;
    }
  }

  //Do an action
  if (readByte > 0) {
    switch (receivedData[0]) {
      case HELLO:   
        valueToSend[0] = HELLO_RESPONSE;
        break;
      case DEACTIVATE:
        isActivated = false;
        turnOffLights();
        break;
      case ACTIVATE:
        isActivated = true;
        turnOnLights();
        break;
      case INIT:
        team = receivedData[1];
        break;
      default:
        debug("Got unknown command", true);
    }
  }

  //Debug
  debug("Read bytes: ", false); debug(readByte, true);
  debug("Command value: ", false); debug((int) receivedData[0], true);
  debug("Sending command value: ", false); debug((int) valueToSend[0], true);
}


/* ____________________________________________________________________________

    void turnOnLights()

    Turns on LEDs with color of current team.
   ____________________________________________________________________________
*/
void turnOnLights() {
  printPixels(teamColors[team]);
}

/* ____________________________________________________________________________

    void turnOffLights()

    Turns off LEDs.
   ____________________________________________________________________________
*/
void turnOffLights() {
  printPixels(teamColors[8]);
}

/* ____________________________________________________________________________

    void sendMessageToA9G(char message[])

    This function stores given message into the valueToSend buffer.
    After that requests sending value by calling requestI2CSend()
   ____________________________________________________________________________
*/
void sendMessageToA9G(char message[]) {
  debug("Sending message: ", false);
  debug(message, true);
 
  uint8_t length = (strlen(message) > I2C_BUFFER_SIZE ? I2C_BUFFER_SIZE : strlen(message));
  for (uint8_t i = 0; i < length + 1; i++) {
    valueToSend[i] = message[i]; 
  }

  requestI2CSend();
  debug("Request sent", true);
}

/* ____________________________________________________________________________

    void requestI2CSend()

    This function quickly triggers interrupt pin on A9G microcontroller. 
    The microcontroller then asks for the I2C data from all sensors.
   ____________________________________________________________________________
*/
void requestI2CSend() {
  pinMode(INTERRUPT_PIN, OUTPUT); //Set to OUTPUT to trigger the bus
  digitalWrite(INTERRUPT_PIN, LOW);
  delay(100);
  digitalWrite(INTERRUPT_PIN, HIGH);
  pinMode(INTERRUPT_PIN, INPUT); //Set back to INPUT to not interfere with the bus
}


/* ____________________________________________________________________________

    void printPixels(uint32_t color)

    This function shows given color on all Neopixel LEDs at full brightness.
   ____________________________________________________________________________
*/
void printPixels(uint32_t color) {
  pixels.clear();
  pixels.setBrightness(100);
  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, color);  
  }
  pixels.show();
}

/* ____________________________________________________________________________

    void debug(char message[], bool ln)

    If preprocessor DEBUG flag is set to 1 then it sends debug messages
    to Serial port. If second parameter is set to true, then it prints
    the message on new line.
   ____________________________________________________________________________
*/
void debug(char message[], bool ln) {
  #if DEBUG >= 1
    if (ln) {
      Serial.println(message);
      return;
    }
    
    Serial.print(message);
  #endif
}

/* ____________________________________________________________________________

    void setup()

    Arduino main setup function. Prepares IR Receiver, LED diodes and I2C library
   ____________________________________________________________________________
*/
void setup()     
{     
  //Initialize Neopixels
  pixels.begin();
  turnOffLights();
  
  //Initialize IR receiver. 
  IrReceiver.begin(IR_RECEIVE_PIN);

  // Initialize Interrupt pin - Set to INPUT to not interfere with the bus. It will be set to output when sending data
  pinMode(INTERRUPT_PIN, INPUT_PULLUP);

  // Initialize I2C library
  Wire.begin(I2C_ADDRESS);
  Wire.onRequest(handleRequest);
  Wire.onReceive(handleReceive);

  //Initialize serial and show debug message
  #if DEBUG >= 1
  Serial.begin(2400);
  Serial.println("Initialize complete");
  #endif
}

/* ____________________________________________________________________________

    void loop()

    Arduino main loop function. Always read IR receiver values and if it 
    receives a value from IR sensor, then it sends it to A9G using
    sendMessageToA9G() function.
   ____________________________________________________________________________
*/
void loop()
{    
  if (isActivated && IrReceiver.decode()) {     // Returns 0 if no data ready, 1 if data ready. 
    uint32_t decodedValue = IrReceiver.decodedIRData.decodedRawData;
    uint32_t c;
    
    uint32_t checkDigits = decodedValue & 0xffffff00;

    if (checkDigits == BYTE_CHECK_MASK) {
      uint8_t lastTwoDigits = decodedValue & 0x000000ff;
      
      char msg[] = {SHOT, lastTwoDigits};
      sendMessageToA9G(msg);  
    }
    
    IrReceiver.resume();  // Restart the ISR state machine and Receive the next value     
  }
}   
